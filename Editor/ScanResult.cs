﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Editor
{
    public class ScanResult
    {
        private int position;
        private int symbolCode;
        private string value;

        public ScanResult()
        {

        }
        public ScanResult(int position, int symbolCode, string value)
        {
            Position = position;
            SymbolCode = symbolCode;
            Value = value;
        }

        public int Position { get => position; set => position = value; }
        public int SymbolCode { get => symbolCode; set => symbolCode = value; }
        public string Value { get => value; set => this.value = value; }
    }
}
