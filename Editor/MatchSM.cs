﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Editor
{
    class MatchSM
    {
        private bool isMatch;
        private int startPosition;
        private int endPosition;
        private string match;
        private List<state> visitedStates;
        public MatchSM()
        {

        }

        public MatchSM(bool isMatch, int startPosition, int endPosition, string match, List<state> visitedStates)
        {
            this.isMatch = isMatch;
            this.startPosition = startPosition;
            this.endPosition = endPosition;
            this.match = match;
            this.visitedStates = new List<state>(visitedStates);
        }

        public string Match { get => match; }
        public bool IsMatch { get => isMatch; }
        public int StartPosition { get => startPosition; }
        public int EndPosition { get => endPosition; }
        internal List<state> VisitedStates { get => visitedStates; }
    }
}
