﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace Editor
{
    class RegEx
    {
        private string sourcetext;
        private string resulttext;
        private string reg;
        public RegEx(string source)
        {
            SourceText = source;
        }
        public void FindTelephon()
        {
            int line = 0;
            int pos = 0;
            string[] strings = SourceText.Split('\n');
            //SourceText = "asfsdf +7(913)980-18-91 sdfdsfdsfdxcv 456313 (12-34-56) 12-34";
            //reg = @"(((\+7)|8)\([0-9]{3,3}\)[0-9]{3,3}-[0-9]{2,2}-[0-9]{2,2})|" + 
            //    @"(((\(\d{3,3}\) \d{3,3}-)|(\(\d{2,2}-\d{2,2}\) \d{2,2}-)|(\(\d{3,3}-\d{2,2}\) \d{1,1}-)|(\(\d{2,2}-\d{2,2}-\d{2,2}\) ))(\d{2,2}-\d{2,2}))";
            reg = @"((((\+7)|8)\([0-9]{3}\) [0-9]{3}-[0-9]{2}-[0-9]{2})|" +
                @"(((\(\d{3}\) \d{3}-)|(\(\d{2}-\d{2}\) \d{2}-)|(\(\d{3}-\d{2}\) \d{1}-)|(\(\d{2}-\d{2}-\d{2}\) ))(\d{2}-\d{2})))($| )";
            Regex regex = new Regex(reg);
            for (int i = 0; i < strings.Length; i++)
            {
                line = i;
                MatchCollection matches = regex.Matches(strings[i]);
                if (matches.Count > 0)
                {
                    foreach (Match match in matches)
                    {
                        pos = match.Index;
                        resulttext += match.Value + " (line:" + (line+1) + ", position:" + (pos+1) + ")" + "\n";
                    }
                }
                else
                {
                    resulttext += "";
                }
            }
        }

        public string SourceText { get => sourcetext; set => sourcetext = value; }
        public string ResultText { get => resulttext; }
    }
}
