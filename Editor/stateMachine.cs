﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Editor
{
    struct state
    {
        public int name;
        public List<transition> transitions;
        public state(int name, List<transition> transitions)
        {
            this.name = name;
            this.transitions = transitions;
        }
    }
    struct transition
    {
        public char sign;
        public state nextState;
        public transition(char sign, state nextState)
        {
            this.nextState = nextState;
            this.sign = sign;
        }
    }
    class stateMachine
    {
        private int[] firstdigit = { 3, 4, 5, 6, 7, 8, 9 };
        private List<state> table = new List<state>();
        private int endState = 34;
        private string resultString;

        public stateMachine()
        {
            createTableTransition();
        }

        private void createTableTransition()
        {
            for(int i = 0; i < endState + 1; i++)
            {
                state state = new state(i, new List<transition>());
                table.Add(state);
            }
  

            //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

            //из 0 состояния
            table[0].transitions.Add(new transition('(', table[1]));
            table[0].transitions.Add(new transition('8', table[23]));
            table[0].transitions.Add(new transition('+', table[22]));
            //--------------

            //из 1 состояния
            table[1].transitions.Add(new transition('F', table[2]));
            //--------------

            //из 2 состояния
            table[2].transitions.Add(new transition('X', table[3]));
            //--------------

            //из 3 состояния
            table[3].transitions.Add(new transition('-', table[4]));
            table[3].transitions.Add(new transition('X', table[15]));
            //--------------

            //из 4 состояния
            table[4].transitions.Add(new transition('X', table[5]));
            //--------------

            //из 5 состояния
            table[5].transitions.Add(new transition('X', table[6]));
            //--------------

            //из 6 состояния
            table[6].transitions.Add(new transition('-', table[7]));
            table[6].transitions.Add(new transition(')', table[8]));
            //--------------

            //из 7 состояния
            table[7].transitions.Add(new transition('X', table[9]));
            //--------------

            //из 8 состояния
            table[8].transitions.Add(new transition(' ', table[12]));
            //--------------

            //из 9 состояния
            table[9].transitions.Add(new transition('X', table[10]));
            //--------------

            //из 10 состояния
            table[10].transitions.Add(new transition(')', table[11]));
            //--------------

            //из 11 состояния
            table[11].transitions.Add(new transition(' ', table[28]));
            //--------------

            //из 12 состояния
            table[12].transitions.Add(new transition('X', table[13]));
            //--------------

            //из 13 состояния
            table[13].transitions.Add(new transition('X', table[14]));
            //--------------

            //из 14 состояния
            table[14].transitions.Add(new transition('-', table[28]));
            //--------------

            //из 15 состояния
            table[15].transitions.Add(new transition(')', table[16]));
            table[15].transitions.Add(new transition('-', table[18]));
            //--------------

            //из 16 состояния
            table[16].transitions.Add(new transition(' ', table[17]));
            //--------------

            //из 17 состояния
            table[17].transitions.Add(new transition('X', table[12]));
            //--------------

            //из 18 состояния
            table[18].transitions.Add(new transition('X', table[19]));
            //--------------

            //из 19 состояния
            table[19].transitions.Add(new transition('X', table[20]));
            //--------------

            //из 20 состояния
            table[20].transitions.Add(new transition(')', table[21]));
            //--------------

            //из 21 состояния
            table[21].transitions.Add(new transition(' ', table[13]));
            //--------------

            //из 22 состояния
            table[22].transitions.Add(new transition('7', table[23]));
            //--------------

            //из 23 состояния
            table[23].transitions.Add(new transition('(', table[24]));
            //--------------

            //из 24 состояния
            table[24].transitions.Add(new transition('F', table[25]));
            //--------------

            //из 25 состояния
            table[25].transitions.Add(new transition('X', table[26]));
            //--------------

            //из 26 состояния
            table[26].transitions.Add(new transition('X', table[27]));
            //--------------

            //из 27 состояния
            table[27].transitions.Add(new transition(')', table[16]));
            //--------------

            //из 28 состояния
            table[28].transitions.Add(new transition('X', table[29]));
            //--------------

            //из 29 состояния
            table[29].transitions.Add(new transition('X', table[30]));
            //--------------

            //из 30 состояния
            table[30].transitions.Add(new transition('-', table[31]));
            //--------------

            //из 31 состояния
            table[31].transitions.Add(new transition('X', table[32]));
            //--------------

            //из 32 состояния
            table[32].transitions.Add(new transition('X', table[33]));
            //--------------

            //из 33 состояния
            table[33].transitions.Add(new transition(' ', table[34]));
            table[33].transitions.Add(new transition('\n', table[34]));
            //--------------

            //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

        }

        public List<MatchSM> FindMatch(string sourceString)
        {
            state nowState;
            List<state> visitedStates = new List<state>();
            sourceString += "\n";
            List<MatchSM> matches = new List<MatchSM>();
            for(int i = 0; i < sourceString.Length; i++)
            {
                resultString = "";
                nowState = table[0];
                int nowPos = i;
                visitedStates.Add(nowState);
                while (nowState.name != endState && nowPos < sourceString.Length)
                {
                    
                    nowState = check(sourceString[nowPos], nowState);
                    if (nowState.name == -1)
                    {
                        MatchSM match = new MatchSM(false, i, (nowPos - 1), resultString.Substring(0, resultString.Length), visitedStates);
                        matches.Add(match);
                        break;
                    }
                    else
                    {
                        resultString += sourceString[nowPos].ToString();
                        nowPos++;
                    }
                    visitedStates.Add(nowState);
                }
                if(nowState.name == endState)
                {
                    MatchSM match = new MatchSM(true, i, (nowPos - 1), resultString.Substring(0, resultString.Length - 1), visitedStates);
                    matches.Add(match);
                    i = nowPos - 1;
                    resultString = "";
                    nowState = table[0];
                }
                nowState = table[0];
                visitedStates.Clear();
                resultString = "";
            }
            return matches;
        }

        private state check(char ch, state nowState)
        {
            for(int i = 0; i < nowState.transitions.Count; i++)
            {
                switch (nowState.transitions[i].sign)
                {
                    case 'X':
                        {
                            for (int j = 0; j < 10; j++)
                            {
                                if (ch.ToString() == j.ToString())
                                {
                                    return nowState.transitions[i].nextState;
                                }
                            }
                            break;
                        }

                    case 'F':
                        {
                            for(int j = 0; j < firstdigit.Length; j++)
                            {
                                if(ch.ToString() == firstdigit[j].ToString())
                                {
                                    return nowState.transitions[i].nextState;
                                }
                            }
                            break;
                        }

                    default:
                        {
                            if (ch == nowState.transitions[i].sign)
                            {
                                return nowState.transitions[i].nextState;
                            }
                            break;
                        }
                }
                //if (nowState.transitions[i].sign == 'X')
                //{
                //    for(int j = 0; j < 10; j++)
                //    {
                //        if(ch.ToString() == j.ToString())
                //        {
                //            return nowState.transitions[i].nextState;
                //        }
                //    }
                //}
                //else
                //{
                //    if (ch == nowState.transitions[i].sign)
                //    {
                //        return nowState.transitions[i].nextState;
                //    }
                //}
            }
            return new state(-1, null);
        }
    }
}
