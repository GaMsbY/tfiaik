﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Editor
{

    public class Scanner
    {

        enum lexemes { id = 0, num = 1, binary = 3, bracket = 4, equal = 5, error = -1 }
        public List<ScanResult> ScannerFunc(string line)
        {
            List<ScanResult> scanResults = new List<ScanResult>();
            string currentIndetif = string.Empty;
            //var result = new ScannerResult();
            string linebackup = line;
            string currentValue = string.Empty;
            char currentLetter = line[0];

            //line = line.Replace(" ", string.Empty);

            for (int i = 0; i < line.Length; i++)
            {
                ScanResult result = new ScanResult();
                currentValue = string.Empty;
                currentLetter = line[i];

                if (line[i].Equals(" ") || line[i] == '\n' || line[i] == ';')
                {
                    continue;
                }

                if (char.IsLetter(line[i]) || (line[i].Equals('_') && char.IsLetter(line[i + 1])))
                {
                    int j = i + 1;

                    result.Position = i;
                    result.SymbolCode = Convert.ToInt32(lexemes.id);
                    currentValue += line[i];

                    while (j < line.Length)
                    {
                        if (char.IsDigit(line[j]) || char.IsLetter(line[j]) || line[j].Equals('_'))
                        {
                            currentValue += line[j];
                            j++;
                        }
                        else
                        {

                            break;
                        }
                    }

                    i = j;
                    result.Value = currentValue;
                    scanResults.Add(result);
                    currentValue = string.Empty;
                }

                if (i >= line.Length)
                {
                    break;
                }

                if (char.IsDigit(line[i]))
                {
                    int j = i + 1;

                    result.Position = i;
                    result.SymbolCode = Convert.ToInt32(lexemes.num);

                    currentValue += line[i];
                    bool onePoint = false;
                    
                    
                    while (j < line.Length)
                    {
                        if (line[j].Equals('.'))
                        {
                            if (!onePoint)
                            {
                                onePoint = true;
                                j++;
                            }
                            else
                            {
                                break;
                            }
                        }
                        if (char.IsDigit(line[j]))
                        {
                            currentValue += line[j];
                            j++;
                        }
                        else
                        {
                            break;
                        }
                    }

                    i = j-1;

                    result.Value = currentValue;
                    scanResults.Add(result);
                    currentValue = string.Empty;
                    continue;
                }

                if (i > line.Length - 1)
                {
                    break;
                }

                if (line[i] == '*' && line[i + 1] == '*')
                {
                    result.Position = i;
                    result.SymbolCode = Convert.ToInt32(lexemes.binary);
                    result.Value = "**";
                    scanResults.Add(result);
                    i = i + 2;
                }

                switch (line[i])
                {
                    case ' ':
                    {
                            continue;
                    }

                    case '+':
                    {
                            result.Position = i;
                            result.SymbolCode = Convert.ToInt32(lexemes.binary);
                            result.Value = "+";
                            scanResults.Add(result);
                            break;
                    }

                    case '-':
                    {
                            result.Position = i;
                            result.SymbolCode = Convert.ToInt32(lexemes.binary);
                            result.Value = "-";
                            scanResults.Add(result);
                            break;
                    }

                    case '/':
                    {
                            result.Position = i;
                            result.SymbolCode = Convert.ToInt32(lexemes.binary);
                            result.Value = "/";
                            scanResults.Add(result);
                            break;
                    }

                    case '*':
                    {
                            result.Position = i;
                            result.SymbolCode = Convert.ToInt32(lexemes.binary);
                            result.Value = "*";
                            scanResults.Add(result);
                            break;
                    }

                    case '(':
                    {
                            result.Position = i;
                            result.SymbolCode = Convert.ToInt32(lexemes.bracket);
                            result.Value = "(";
                            scanResults.Add(result);
                            break;
                    }

                    case ')':
                    {
                            result.Position = i;
                            result.SymbolCode = Convert.ToInt32(lexemes.bracket);
                            result.Value = ")";
                            scanResults.Add(result);
                            break;
                    }

                    case '=':
                    {
                            result.Position = i;
                            result.SymbolCode = Convert.ToInt32(lexemes.equal);
                            result.Value = "=";
                            scanResults.Add(result);
                            break;
                    }
               
                    default:
                    {
                            result.Position = i;
                            result.SymbolCode = Convert.ToInt32(lexemes.error);
                            result.Value = line[i].ToString();
                            scanResults.Add(result);
                            break;
                    }
                }
                if (i > line.Length - 1)
                {
                    break;
                }
            }
            return scanResults;
        }
    }
}
