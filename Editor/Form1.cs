﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace Editor
{
    public partial class Form1 : Form
    {
        string defaultFileName = "new.txt";

        public Form1()
        {
            InitializeComponent();
        }

        void Create()
        {
            TabPage newTabPage = new TabPage();
            newTabPage.Size = enter_tc.Size;
            RichTextBox newRichTextBox = new RichTextBox();
            //newRichTextBox.Size = newTabPage.Size;
            newRichTextBox.Width = newTabPage.Width - 5;
            newRichTextBox.Height = newTabPage.Height - 30;
            newTabPage.Controls.Add(newRichTextBox);
            newTabPage.Text = defaultFileName;
            enter_tc.TabPages.Add(newTabPage);

            TabPage newTabPageResult = new TabPage();
            newTabPageResult.Size = result_tc.Size;
            RichTextBox newRichTextBoxRresult = new RichTextBox();
            //newRichTextBoxRresult.Size = newTabPageResult.Size;
            newRichTextBoxRresult.Width = newTabPageResult.Width - 5;
            newRichTextBoxRresult.Height = newTabPageResult.Height - 30;
            newRichTextBoxRresult.ReadOnly = true;
            newTabPageResult.Controls.Add(newRichTextBoxRresult);
            newTabPageResult.Text = defaultFileName;
            result_tc.TabPages.Add(newTabPageResult);
            enter_tc.SelectedIndex = enter_tc.TabCount - 1;
            result_tc.SelectedIndex = result_tc.TabCount - 1;
        }

        void Open()
        {
            if (openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                using (FileStream fstream = File.OpenRead(openFileDialog1.FileName))
                {
                    // преобразуем строку в байты
                    byte[] array = new byte[fstream.Length];
                    // считываем данные
                    fstream.Read(array, 0, array.Length);
                    // декодируем байты в строку
                    string textFromFile = System.Text.Encoding.Default.GetString(array);

                    TabPage newTabPage = new TabPage();
                    newTabPage.Size = enter_tc.Size;
                    RichTextBox newRichTextBox = new RichTextBox();
                    newRichTextBox.Width = newTabPage.Width - 5;
                    newRichTextBox.Height = newTabPage.Height - 25;
                    newRichTextBox.Text = textFromFile;
                    newTabPage.Controls.Add(newRichTextBox);
                    newTabPage.Text = openFileDialog1.FileName;
                    enter_tc.TabPages.Add(newTabPage);
                }
                TabPage newTabPageResult = new TabPage();
                newTabPageResult.Size = result_tc.Size;
                RichTextBox newRichTextBoxRresult = new RichTextBox();
                //newRichTextBoxRresult.Size = newTabPageResult.Size;
                newRichTextBoxRresult.Width = newTabPageResult.Width - 5;
                newRichTextBoxRresult.Height = newTabPageResult.Height - 25;
                newRichTextBoxRresult.ReadOnly = true;
                newTabPageResult.Controls.Add(newRichTextBoxRresult);
                newTabPageResult.Text = openFileDialog1.FileName;
                result_tc.TabPages.Add(newTabPageResult);
                enter_tc.SelectedIndex = enter_tc.TabCount - 1;
                result_tc.SelectedIndex = result_tc.TabCount - 1;
            }
        }

        void SaveAs()
        {
            saveFileDialog1.FileName = enter_tc.SelectedTab.Text;
            saveFileDialog1.InitialDirectory = Directory.GetCurrentDirectory();
            if (saveFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                using (FileStream fstream = new FileStream(saveFileDialog1.FileName, FileMode.Create))
                {
                    // преобразуем строку в байты
                    byte[] array = System.Text.Encoding.Default.GetBytes(enter_tc.SelectedTab.GetChildAtPoint(new Point(1, 1)).Text);
                    // запись массива байтов в файл
                    fstream.Write(array, 0, array.Length);
                    enter_tc.SelectedTab.Text = saveFileDialog1.FileName;
                    result_tc.SelectedTab.Text = saveFileDialog1.FileName;
                }
            }
        }

        void Save()
        {
            try
            {
                if (enter_tc.SelectedTab.Text == "new.txt")
                {
                    SaveAs();
                }
                else
                {
                    using (FileStream fstream = new FileStream(enter_tc.SelectedTab.Text, FileMode.Create))
                    {
                        // преобразуем строку в байты
                        byte[] array = System.Text.Encoding.Default.GetBytes(enter_tc.SelectedTab.GetChildAtPoint(new Point(1, 1)).Text);
                        // запись массива байтов в файл
                        fstream.Write(array, 0, array.Length);
                    }
                }
            }
            catch(NullReferenceException error)
            {

            }
        }

        private void CreateToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Create();
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            Create();
        }

        private void OpenToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Open();
        }

        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            Open();
        }

        private void SaveAsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveAs();

        }

        private void SaveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Save();
        }

        private void toolStripButton3_Click(object sender, EventArgs e)
        {
            Save();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void backToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                RichTextBox selectedTextBox = (RichTextBox)enter_tc.SelectedTab.GetChildAtPoint(new Point(1, 1));
                selectedTextBox.Undo();
            }
            catch (NullReferenceException error)
            {

            }
        }

        private void toolStripButton4_Click(object sender, EventArgs e)
        {
            try
            {
                RichTextBox selectedTextBox = (RichTextBox)enter_tc.SelectedTab.GetChildAtPoint(new Point(1, 1));
                selectedTextBox.Undo();
            }
            catch(NullReferenceException error)
            {

            }
        }

        private void nextToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                RichTextBox selectedTextBox = (RichTextBox)enter_tc.SelectedTab.GetChildAtPoint(new Point(1, 1));
                selectedTextBox.Redo();
            }
            catch (NullReferenceException error)
            {

            }
        }

        private void toolStripButton5_Click(object sender, EventArgs e)
        {
            try
            {
                RichTextBox selectedTextBox = (RichTextBox)enter_tc.SelectedTab.GetChildAtPoint(new Point(1, 1));
                selectedTextBox.Redo();
            }
            catch (NullReferenceException error)
            {

            }
        }

        private void copyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                RichTextBox selectedTextBox = (RichTextBox)enter_tc.SelectedTab.GetChildAtPoint(new Point(1, 1));
                selectedTextBox.Copy();
            }
            catch (NullReferenceException error)
            {

            }
        }

        private void toolStripButton6_Click(object sender, EventArgs e)
        {
            try
            {
                RichTextBox selectedTextBox = (RichTextBox)enter_tc.SelectedTab.GetChildAtPoint(new Point(1, 1));
                selectedTextBox.Copy();
            }
            catch (NullReferenceException error)
            {

            }
        }

        private void insertToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                RichTextBox selectedTextBox = (RichTextBox)enter_tc.SelectedTab.GetChildAtPoint(new Point(1, 1));
                selectedTextBox.Paste();
            }
            catch (NullReferenceException error)
            {

            }
        }

        private void toolStripButton8_Click(object sender, EventArgs e)
        {
            try
            {
                RichTextBox selectedTextBox = (RichTextBox)enter_tc.SelectedTab.GetChildAtPoint(new Point(1, 1));
                selectedTextBox.Paste();
            }
            catch (NullReferenceException error)
            {

            }
        }

        private void cutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                RichTextBox selectedTextBox = (RichTextBox)enter_tc.SelectedTab.GetChildAtPoint(new Point(1, 1));
                selectedTextBox.Cut();
            }
            catch (NullReferenceException error)
            {

            }
        }

        private void toolStripButton7_Click(object sender, EventArgs e)
        {
            try
            {
                RichTextBox selectedTextBox = (RichTextBox)enter_tc.SelectedTab.GetChildAtPoint(new Point(1, 1));
                selectedTextBox.Cut();
            }
            catch (NullReferenceException error)
            {

            }
        }

        private void deleteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                RichTextBox selectedTextBox = (RichTextBox)enter_tc.SelectedTab.GetChildAtPoint(new Point(1, 1));
                selectedTextBox.Clear();
            }
            catch (NullReferenceException error)
            {

            }
        }

        private void highliteAllToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                RichTextBox selectedTextBox = (RichTextBox)enter_tc.SelectedTab.GetChildAtPoint(new Point(1, 1));
                selectedTextBox.SelectAll();
            }
            catch (NullReferenceException error)
            {

            }
        }

        private void enter_tc_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                int index = enter_tc.SelectedIndex;
                result_tc.SelectTab(index);
            }
            catch(ArgumentOutOfRangeException error)
            {

            }
        }

        private void toolStripButton9_Click(object sender, EventArgs e)//закрытие вкладки
        {
            DialogResult result = MessageBox.Show(
                "Сохранить при закрытии?",
                "Закрытие файла",
                MessageBoxButtons.YesNoCancel,
                MessageBoxIcon.Information,
                MessageBoxDefaultButton.Button1);
            if(result == DialogResult.Yes)
            {
                Save();
                var tabIndexClose = result_tc.SelectedIndex;
                result_tc.TabPages.RemoveAt(tabIndexClose);
                enter_tc.TabPages.RemoveAt(tabIndexClose);
                if (tabIndexClose == 0)
                {
                    enter_tc.SelectedIndex = 0;
                    result_tc.SelectedIndex = 0;
                }
                else
                {
                    enter_tc.SelectedIndex = tabIndexClose - 1;
                    result_tc.SelectedIndex = tabIndexClose - 1;
                }
            }
            if (result == DialogResult.No)
            {
                var tabIndexClose = result_tc.SelectedIndex;
                result_tc.TabPages.RemoveAt(tabIndexClose);
                enter_tc.TabPages.RemoveAt(tabIndexClose);
                if (tabIndexClose == 0)
                {
                    enter_tc.SelectedIndex = 0;
                    result_tc.SelectedIndex = 0;
                }
                else
                {
                    enter_tc.SelectedIndex = tabIndexClose - 1;
                    result_tc.SelectedIndex = tabIndexClose - 1;
                }
            }
            if (result == DialogResult.Cancel)
            {
                
            }
            
        }

        private void enter_tc_ControlRemoved(object sender, ControlEventArgs e)
        {
            if(enter_tc.TabPages.Count == 1)
            {
                toolStripButton3.Enabled = false;
                toolStripButton4.Enabled = false;
                toolStripButton5.Enabled = false;
                toolStripButton6.Enabled = false;
                toolStripButton7.Enabled = false;
                toolStripButton8.Enabled = false;
                toolStripButton9.Enabled = false;

                SaveToolStripMenuItem.Enabled = false;
                SaveAsToolStripMenuItem.Enabled = false;
                правкаToolStripMenuItem.Enabled = false;
                regExToolStripMenuItem.Enabled = false;
                StateMToolStripMenuItem.Enabled = false;
            }
        }

        private void enter_tc_ControlAdded(object sender, ControlEventArgs e)
        {
            toolStripButton3.Enabled = true;
            toolStripButton4.Enabled = true;
            toolStripButton5.Enabled = true;
            toolStripButton6.Enabled = true;
            toolStripButton7.Enabled = true;
            toolStripButton8.Enabled = true;
            toolStripButton9.Enabled = true;

            SaveToolStripMenuItem.Enabled = true;
            SaveAsToolStripMenuItem.Enabled = true;
            правкаToolStripMenuItem.Enabled = true;
            regExToolStripMenuItem.Enabled = true;
            StateMToolStripMenuItem.Enabled = true;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            openFileDialog1.Filter = "TXT File (*.txt)|*.txt";
            saveFileDialog1.Filter = "TXT File (*.txt)|*.txt";
            toolStripButton3.Enabled = false;
            toolStripButton4.Enabled = false;
            toolStripButton5.Enabled = false;
            toolStripButton6.Enabled = false;
            toolStripButton7.Enabled = false;
            toolStripButton8.Enabled = false;
            toolStripButton9.Enabled = false;

            SaveToolStripMenuItem.Enabled = false;
            SaveAsToolStripMenuItem.Enabled = false;
            правкаToolStripMenuItem.Enabled = false;
            regExToolStripMenuItem.Enabled = false;
            StateMToolStripMenuItem.Enabled = false;
            //openFileDialog1.Filter = "All files|*.*";

            this.Width = 1700;
            this.Height = 900;
            this.MinimumSize = new Size(600, 500);
            enter_tc.Width = this.Width - 42;
            enter_tc.Height = this.Height - 300;

            result_tc.Left = 13;
            result_tc.Top = enter_tc.Height + 65;
            result_tc.Width = this.Width - 42;
        }

        private void AboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AboutBox aboutBox = new AboutBox();
            aboutBox.ShowDialog();
        }

        private void helpCallToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Help.ShowHelp(this, @"D:\Desktop\ТФЯиК\Лабораторные\Help.chm");
        }

        private void result_tc_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                int index = result_tc.SelectedIndex;
                enter_tc.SelectTab(index);
            }
            catch (ArgumentOutOfRangeException error)
            {

            }
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (enter_tc.TabPages.Count != 0)
            {
                DialogResult result = MessageBox.Show(
                    "Сохранить?",
                    "Закрытие программы",
                    MessageBoxButtons.YesNo,
                    MessageBoxIcon.Information,
                    MessageBoxDefaultButton.Button1,
                    MessageBoxOptions.DefaultDesktopOnly);
                if (result == DialogResult.Yes)
                {
                    for (int i = 0; i < enter_tc.TabPages.Count; i++)
                    {
                        enter_tc.SelectTab(i);
                        Save();
                    }
                }
            }
        }

        private void Form1_Resize(object sender, EventArgs e)
        {
            enter_tc.Width = this.Width - 42;
            enter_tc.Height = this.Height - 300;

            result_tc.Left = 13;
            result_tc.Top = enter_tc.Height + 60;
            result_tc.Width = this.Width - 42;
        }


        private void DeleteAllToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                RichTextBox selectedTextBox = (RichTextBox)enter_tc.SelectedTab.GetChildAtPoint(new Point(1, 1));
                selectedTextBox.SelectAll();
                selectedTextBox.Clear();
            }
            catch (NullReferenceException error)
            {

            }
        }

        private void regExToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            string text = enter_tc.SelectedTab.GetChildAtPoint(new Point(1, 1)).Text;
            RegEx regEx = new RegEx(text);
            regEx.FindTelephon();
            string result = regEx.ResultText;
            if (result.Length != 0)
            {
                result_tc.SelectedTab.GetChildAtPoint(new Point(1, 1)).Text = result;
            }
            else
            {
                result_tc.SelectedTab.GetChildAtPoint(new Point(1, 1)).Text = "Ничего не найдено!";
            }
        }

        private void StateMToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            result_tc.SelectedTab.GetChildAtPoint(new Point(1, 1)).Text = "";
            string text = enter_tc.SelectedTab.GetChildAtPoint(new Point(1, 1)).Text;
            string[] strings = text.Split('\n');
            for (int i = 0; i < strings.Length; i++)
            {
                stateMachine stateMachine = new stateMachine();
                var matches = stateMachine.FindMatch(strings[i]);
                if (matches.Count > 0)
                {
                    foreach (MatchSM match in matches)
                    {
                        string states = "(";
                        for (int j = 0; j < match.VisitedStates.Count; j++)
                        {
                            if (j != 0)
                            {
                                states += ", ";
                            }
                            states += match.VisitedStates[j].name.ToString();
                        }
                        states += ")";
                        result_tc.SelectedTab.GetChildAtPoint(new Point(1, 1)).Text += match.Match + " " + match.IsMatch.ToString() + " (line:" + (i + 1) + ", position:" + (match.StartPosition + 1) + ") " + states + "\n";
                    }
                }
                else
                {
                    result_tc.SelectedTab.GetChildAtPoint(new Point(1, 1)).Text += "";
                }
                if (result_tc.SelectedTab.GetChildAtPoint(new Point(1, 1)).Text == "")
                {
                    result_tc.SelectedTab.GetChildAtPoint(new Point(1, 1)).Text = "Ничего не найдено!";
                }
            }
        }

        private void ScannerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string enterString = enter_tc.SelectedTab.GetChildAtPoint(new Point(1, 1)).Text;
            List<ScanResult> scanResults = new List<ScanResult>();
            Scanner scanner = new Scanner();
            scanResults = scanner.ScannerFunc(enterString);
            string result = string.Empty;
            for(int i = 0; i < scanResults.Count; i++)
            {
                result += "Position: " + scanResults[i].Position + " Type: " + scanResults[i].SymbolCode + " Value: " + scanResults[i].Value + "\n";
            }
            result_tc.SelectedTab.GetChildAtPoint(new Point(1, 1)).Text = result;
        }
    }
}
